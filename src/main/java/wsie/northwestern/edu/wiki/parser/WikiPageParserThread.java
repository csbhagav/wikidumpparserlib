package wsie.northwestern.edu.wiki.parser;

import wsie.northwestern.edu.wiki.DAO.WikiPage;

/**
 * @author csbhagav
 */
public class WikiPageParserThread implements Runnable {

    WikiPage page;

    /**
     * Users of this library must override this method.
     */
    public void run() {
        System.out.println(getPage().getId());
    }

    public WikiPage getPage() {
        return page;
    }

    public void setPage(WikiPage page) {
        this.page = page;
    }

    public WikiPageParserThread newInstance() {
        return new WikiPageParserThread();
    }

}
