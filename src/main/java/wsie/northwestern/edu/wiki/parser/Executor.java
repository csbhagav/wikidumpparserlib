package wsie.northwestern.edu.wiki.parser;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.xerces.impl.io.MalformedByteSequenceException;
import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;

/**
 * @author csbhagav
 *         <p/>
 *         This class provides an entry point into the library code.
 *         <p/>
 *         This class exposes a static method that parses the given file using
 *         the SAX parser.
 *         <p/>
 *         To use the library, a developer needs to call the static function
 *         startParser
 */
public class Executor {
    /**
     * @param numThreads number of threads used for processing
     * @param fileName   Wikipedia dump file to parse (pages-article*.xml)
     * @param runClass   An object of a class that extends {@link WikiPageParserThread}
     */
    public static void readFile(String fileName, Integer numThreads,
                                WikiPageParserThread runClass) {

        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser parser = spf.newSAXParser();

            System.out.println("Reading text from file stream : " + fileName);
            InputStream in = new FileInputStream(fileName);
            InputSource source = new InputSource(in);
            TagHandler tagHandler = new TagHandler(numThreads, runClass);

            parser.parse(source, tagHandler);
        } catch (FileNotFoundException e) {
            System.out.println("Unable to find file - I/O exception");
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            System.out.println("Unable to initialize SAX parser");
            e.printStackTrace();
        } catch (SAXException e) {
            System.out.println("Unable parse using SAX parser");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Unable to find file - I/O exception");
            e.printStackTrace();
        }
    }

    public static void readFile(String fileName, Integer numThreads,
                                WikiPageParserThread runClass, Boolean isCompressed) {


        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
//            SAXParser parser = spf.newSAXParser();
            SAXParser parser = new SAXParser();
            System.out.println("Reading text from file stream : " + fileName);
            InputStream in = new FileInputStream(fileName);

            if (isCompressed) {
                BZip2CompressorInputStream bsip2stream = new BZip2CompressorInputStream(in);
                InputStreamReader isr = new InputStreamReader(bsip2stream, "UTF-8");
                InputSource source = new InputSource(isr);
                TagHandler tagHandler = new TagHandler(numThreads, runClass);
                parser.setContentHandler(tagHandler);
                parser.parse(source);
            } else {
                InputStreamReader isr = new InputStreamReader(in, "UTF-8");
                InputSource source = new InputSource(isr);
                TagHandler tagHandler = new TagHandler(numThreads, runClass);
                parser.setContentHandler(tagHandler);
                parser.parse(source);
            }


        } catch (FileNotFoundException e) {
            System.out.println("Unable to find file - I/O exception");
            e.printStackTrace();
            System.out.println("ReadFile FAILED for " + fileName);
        } catch (ParserConfigurationException e) {
            System.out.println("Unable to initialize SAX parser");
            e.printStackTrace();
            System.out.println("ReadFile FAILED for " + fileName);
        } catch (SAXException e) {
            System.out.println("Unable parse using SAX parser");
            e.printStackTrace();
            System.out.println("ReadFile FAILED for " + fileName);
        } catch (MalformedByteSequenceException e) {
            System.out.println("Malformed Seq ... .\n" + e.getMessage());
            e.printStackTrace();
            System.out.println("ReadFile FAILED for " + fileName);
        } catch (IOException e) {
            System.out.println("Unable to find file - I/O exception");
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array Index out of bounds.\n" + e.getMessage());
            e.printStackTrace();
            System.out.println("ReadFile FAILED for " + fileName);
        }
    }

}
