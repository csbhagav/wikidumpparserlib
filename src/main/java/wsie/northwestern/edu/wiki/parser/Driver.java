package wsie.northwestern.edu.wiki.parser;

import org.apache.commons.cli.*;


public class Driver {

    /**
     * @param args
     * @author csbhagav
     * <p/>
     * This class allows command line execution of the program. This is
     * only for test purposes. Not a part of the Library
     */
    public static void main(String[] args) {

        CommandLineParser cp = new GnuParser();
        HelpFormatter formatter = new HelpFormatter();
        Options opts = getOptions();
        CommandLine cl;
        String dumpFile = "";
        try {
            cl = cp.parse(opts, args);
            Integer numThreads = 1;
//            dumpFile = "/Users/csbhagav/Projects/Data/tempWikiData/temp.xml";
            dumpFile = "/Users/csbhagav/Projects/Data/wikipediaData/enwiki-20131001-pages-articles25" +
                    ".xml-p023725001p026624997.bz2";

            Executor.readFile(dumpFile, numThreads, new WikiPageParserThread(), true);

        } catch (ParseException e) {
            System.out.println("Unable to parse input arguments");

            formatter.printHelp("java -jar wikiDumpParse.jar", opts);
        }

    }

    private static Options getOptions() {

        Options opts = new Options();
        Option threadOpt = new Option("t", "numThreads", true,
                "Number of threads to be used for parsing. Default = 1");
        threadOpt.setRequired(false);
        opts.addOption(threadOpt);

        Option fileOpt = new Option("i", "input", true,
                "XML dump file to be parsed");
        fileOpt.setRequired(false);
        opts.addOption(fileOpt);

        return opts;

    }

}
