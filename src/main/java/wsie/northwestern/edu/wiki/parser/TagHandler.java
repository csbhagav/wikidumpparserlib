package wsie.northwestern.edu.wiki.parser;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import wsie.northwestern.edu.wiki.DAO.WikiContributor;
import wsie.northwestern.edu.wiki.DAO.WikiPage;
import wsie.northwestern.edu.wiki.DAO.WikiRevision;

import javax.xml.parsers.ParserConfigurationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author csbhagav
 * 
 *         This class extends the SAX DefaultHandler.
 * 
 *         It creates an object of the type {@link WikiPageParserThread} and
 *         executes its run method
 * 
 * 
 */
public class TagHandler extends DefaultHandler {

	Integer numThreads = 1;
	WikiPageParserThread runClass = null;
	@SuppressWarnings("rawtypes")
	Class parserThreadClass = null;
	private String buf = "";

	private WikiPage page = null;
	private WikiRevision revision = null;
	private WikiContributor contributor = null;

	private ExecutorService exec;

	private final String PAGE_TAG = "page";
	private final String NS_TAG = "ns";
	private final String TITLE_TAG = "title";
	private final String PAGE_ID_TAG = "id";
	private final String REDIRECT_TAG = "redirect";
	private final String REVISION_TAG = "revision";
	private final String REVISION_ID_TAG = "id";
	private final String TIMESTAMP_TAG = "timestamp";
	private final String CONTRIBUTOR_TAG = "contributor";
	private final String USERNAME_TAG = "username";
	private final String CONTRIBUTOR_ID_TAG = "id";
	private final String COMMENT_TAG = "comment";
	private final String TEXT_TAG = "text";
	private final String END_TAG = "mediawiki";

	private Boolean inPage = false;
	private Boolean inNs = false;
	private Boolean inTitle = false;
	private Boolean inPageId = false;
	private Boolean inRedirect = false;
	private Boolean inRevision = false;
	private Boolean inRevisionId = false;
	private Boolean inTimestamp = false;
	private Boolean inContributor = false;
	private Boolean inUsername = false;
	private Boolean inContributorId = false;
	private Boolean inComment = false;
	private Boolean inText = false;

	public TagHandler(Integer numThreads, WikiPageParserThread runClass)
			throws ParserConfigurationException {
		this.numThreads = numThreads;
		this.exec = Executors.newFixedThreadPool(numThreads);
		this.runClass = runClass;
		parserThreadClass = runClass.getClass();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) {

		if (qName.equalsIgnoreCase(PAGE_TAG)) {
			page = new WikiPage();
			revision = new WikiRevision();
			contributor = new WikiContributor();
			runClass = runClass.newInstance();
			revision.setContributor(contributor);
			page.setRevision(revision);
			inPage = true;
		} else if (qName.equalsIgnoreCase(NS_TAG)) {
			inNs = true;
		} else if (qName.equalsIgnoreCase(TITLE_TAG)) {
			inTitle = true;
		} else if (qName.equalsIgnoreCase(PAGE_ID_TAG) && inPage && !inRevision) {
			inPageId = true;
		} else if (qName.equalsIgnoreCase(REDIRECT_TAG)) {
			inRedirect = true;
			page.setIsRedirect(true);
			page.setRedirectTitle(attributes.getValue("title"));
		} else if (qName.equalsIgnoreCase(REVISION_TAG)) {
			inRevision = true;
		} else if (qName.equalsIgnoreCase(REVISION_ID_TAG) && inPage
				&& inRevision && !inContributor) {
			inRevisionId = true;
		} else if (qName.equalsIgnoreCase(TIMESTAMP_TAG)) {
			inTimestamp = true;
		} else if (qName.equalsIgnoreCase(CONTRIBUTOR_TAG)) {
			inContributor = true;
		} else if (qName.equalsIgnoreCase(USERNAME_TAG)) {
			inUsername = true;
		} else if (qName.equalsIgnoreCase(CONTRIBUTOR_ID_TAG) && inContributor) {
			inContributorId = true;
		} else if (qName.equalsIgnoreCase(COMMENT_TAG)) {
			inComment = true;
		} else if (qName.equalsIgnoreCase(TEXT_TAG)) {
			inText = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (qName.equalsIgnoreCase(PAGE_TAG)) {
			inPage = false;
			runClass.setPage(page);
			exec.submit(runClass);
		} else if (qName.equalsIgnoreCase(NS_TAG)) {
			inNs = false;
			page.setNamespace(Integer.valueOf(buf));
            buf = "";
		} else if (qName.equalsIgnoreCase(TITLE_TAG)) {
			page.setTitle(buf);
			buf = "";
			inTitle = false;
		} else if (qName.equalsIgnoreCase(PAGE_ID_TAG) && inPage && !inRevision) {
			page.setId(buf);
			buf = "";
			inPageId = false;
		} else if (qName.equalsIgnoreCase(REDIRECT_TAG)) {
			// page.setRedirect(buf);
			buf = "";
			inRedirect = false;
		} else if (qName.equalsIgnoreCase(REVISION_TAG)) {
			inRevision = false;
		} else if (qName.equalsIgnoreCase(REVISION_ID_TAG) && inPage
				&& inRevision && !inContributor) {
			page.getRevision().setId(buf);
			buf = "";
			inRevisionId = false;
		} else if (qName.equalsIgnoreCase(TIMESTAMP_TAG)) {
			page.getRevision().setTimeStamps(buf);
			buf = "";
			inTimestamp = false;
		} else if (qName.equalsIgnoreCase(CONTRIBUTOR_TAG)) {
			inContributor = false;
		} else if (qName.equalsIgnoreCase(USERNAME_TAG)) {
			page.getRevision().getContributor().setUserName(buf);
			buf = "";
			inUsername = false;
		} else if (qName.equalsIgnoreCase(CONTRIBUTOR_ID_TAG) && inContributor) {
			page.getRevision().getContributor().setId(buf);
			buf = "";
			inContributorId = false;
		} else if (qName.equalsIgnoreCase(COMMENT_TAG)) {
			page.getRevision().setComment(buf);
			buf = "";
			inComment = false;
		} else if (qName.equalsIgnoreCase(TEXT_TAG)) {
			page.getRevision().setText(buf);
			buf = "";
			inText = false;
		} else if (qName.equalsIgnoreCase(END_TAG)) {
			exec.shutdown();
			while (!exec.isTerminated()) {
			}
			System.out.println("Terminated all threads !!");
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {

		if (inTitle || inPageId || inRedirect || inRevisionId || inTimestamp
				|| inUsername || inContributorId || inComment || inText || inNs)
			buf += String.copyValueOf(ch, start, length);
	}

	// private T getInstanceOfT(Class<T> clazz) throws InstantiationException,
	// IllegalAccessException {
	// return clazz.newInstance();
	//
	// }

}
