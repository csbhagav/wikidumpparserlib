package wsie.northwestern.edu.wiki.DAO;

/**
 * 
 * @author csbhagav
 * 
 *         This class holds data for a Wikipedia page, including the titl, id,
 *         redirect and revision information
 */
public class WikiPage {

	String title;
	Integer namespace;
	String id;
	String redirectTitle;
	Boolean isRedirect = false;
	WikiRevision revision;

	public WikiPage(String title, String id, WikiRevision revision) {
		super();
		this.title = title;
		this.id = id;
		this.revision = revision;
	}

	public WikiPage() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getNamespace() {
		return namespace;
	}

	public void setNamespace(Integer namespace) {
		this.namespace = namespace;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public WikiRevision getRevision() {
		return revision;
	}

	public void setRevision(WikiRevision revision) {
		this.revision = revision;
	}

	public String getRedirectTitle() {
		return redirectTitle;
	}

	public void setRedirectTitle(String redirect) {
		this.redirectTitle = redirect;
	}

	public Boolean getIsRedirect() {
		return isRedirect;
	}

	public void setIsRedirect(Boolean isRedirect) {
		this.isRedirect = isRedirect;
	}

	public String toString() {
		String retStr = "";
		retStr += "Title:" + title + "\n";
		retStr += "ID:" + id + "\n";
		retStr += "Redirect:" + redirectTitle + "\n";
		retStr += "Revision:" + revision.toString() + "\n";
		return retStr;
	}

}
