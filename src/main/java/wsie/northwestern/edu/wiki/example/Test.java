package wsie.northwestern.edu.wiki.example;

import wsie.northwestern.edu.wiki.parser.WikiPageParserThread;

/**
 * 
 * @author csbhagav
 * 
 *         Example class that extends {@link WikiPageParserThread}
 */
public class Test extends WikiPageParserThread {

	public void run() {

		System.out.println(getPage().getId());

	}

}
